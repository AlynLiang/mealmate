import { useEffect, useState } from 'react';
import './App.css';
import Box from './Box.js';

function App() {
  return (
    <div>
      <h1>Hello world</h1>
      <Box />
    </div>
  );
}

export default App;
